<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
  protected $table = 'job';

  protected $fillable = ['title', 'description', 'email', 'publish'];
  public $timestamps = false;
}
