<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Job;
use Mail;
use Input;
use URL;

class UserController extends Controller
{
  /**
   * Create a new controller instance.
   *
   * @return void
   */
  public function __construct()
  {

  }

  /**
   * Show the application dashboard.
   *
   * @return \Illuminate\Contracts\Support\Renderable
   */

//User Story 1
   public function jobSubmissionPage(Request $request)
   {
     if($request->isMethod('post'))
     {
        $hr = User::where('type', 1)->get();
        $moderator = User::where('type', 2)->get();
        $email = $request->input('email');

        $job_data = array();
        if(User::where('email', $request->get('email'))->exists())
        {
          $job_data = [
           'title' => $request->input('title'),
           'description' => $request->input('description'),
           'email' =>  $request->input('email'),
           'publish' => true,
          ];
          $job = Job::create($job_data);
        }else{
          $job_data = [
           'title' => $request->input('title'),
           'description' => $request->input('description'),
           'email' =>  $request->input('email'),
           'publish' => false,
           ];
          $job = Job::create($job_data);

          foreach($moderator as $m)
          {
            $data = array('sendto' => $m->email,
                          'title' => $request->get('title'),
                          'description' => $request->get('description'),
                          'email' => $request->get('email'),
                          'id' => $job->id,
                          'link' => ('/publishedJob'),
                          'spam' => ('/spam')
                        );
            Mail::send('viewmail', ['data' => $data], function ($message) use ($data) {
                  $message->subject('Job');
                  $message->to($data['sendto'], $data['title'], $data['description'], $data['email'], $data['id'], $data['link'],$data['spam']);
                });
              }
            }
          $jobs = Job::all();
          return view('posts', ['jobs'=>$jobs]);
        }
      return view('jobSubmissionPage');
    }

    public function publishedJob(Request $request)
    {
      $job = Job::findOrFail($request->id);
      $job->publish = true;
      $job->save();

      //create hr
      $user_data = array();
      $user_data=[
        'name' => '',
        'email' =>  $request->input('email'),
        'password' => 'pass',
         'type' => 1,
      ];
      $user = User::create($user_data);

      $jobs = Job::all();
      return view('posts', ['jobs'=>$jobs]);
    }


    public function spam(Request $request)
    {
      return view('spam');
    }

   public function posts()
   {
     return view('posts');
   }

}
