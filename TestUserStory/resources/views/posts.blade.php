@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Jobs:</div>

                <div class="card-body">
                    <a href="/jobSubmissionPage" >Job Submission Page </a>
                  <table class="table">
                 <thead>
                   <tr>
                     <th>Title:</th>
                     <th>Descripton</th>
                   </tr>
                 <tbody>
                     @foreach($jobs as $job)
                   <tr>
                     <td>{{$job->title}}</td>
                     <td>{{$job->description}}</td>
                   </tr>
                     @endforeach
                 </tbody>
                </div>

            </div>
        </div>
    </div>
</div>
@endsection
