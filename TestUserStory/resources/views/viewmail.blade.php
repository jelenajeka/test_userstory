@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">

                  <input class="form-control" name="id" value="{{$data['id']}}" hidden>
                  <br>
                  <label class="formControlRange">Title:</label>
                    <p class="form-control" name="title" >{{$data['title']}}</p>
                  <label class="formControlRange">Description:</label>
                    <p type="text" class="form-control" name="description">{{$data['description']}}</p>
                  <label class="formControlRange">EMail:</label>
                    <p class="form-control" name="email">{{$data['email']}}</p>
                    <a href="{{$data['link']}}">Yes</a>
                  <!-- <a href="/publishedJob">Yes</a> -->
                  <a href="{{$data['spam']}}">Spam</a>
                  <!-- <a href="/spam">No</a> -->
                </div>
              </div>
            </div>
        </div>
    </div>
</div>
@endsection
