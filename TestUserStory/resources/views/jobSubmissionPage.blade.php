@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dodaj proizvod</div>

                <div class="card-body">
                  <form action="/jobSubmissionPage" class="form form-group" method="POST">
                  @csrf
                    <label class="formControlRange">Title:</label>
                    <input class="form-control" name="title">
                    <label class="formControlRange">Description:</label>
                    <input type="text" class="form-control" name="description">
                    <label class="formControlRange">EMail:</label>
                    <input class="form-control" name="email">
                    <button class="form-control" type="submit">Submit</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
